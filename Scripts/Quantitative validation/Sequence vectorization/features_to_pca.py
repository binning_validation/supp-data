#!/usr/bin/env python

from sklearn.decomposition import PCA
import pandas as pd
import numpy as np

def pca_signatures(signature_file,outfile):
    contigs = pd.read_csv(signature_file,header=0,index_col=0)
    #add pseudo count
    contigs_idx = contigs.index
    contigs = contigs.values
    contigs += 1
    #Normalize kmer frequencies to remove effect of contig length
    log_contigs = np.log(contigs / contigs.sum(axis=1,keepdims=True))
    df_log_contigs = pd.DataFrame(log_contigs,index=contigs_idx)
    pca = PCA()
    pca.fit(df_log_contigs)
    scores = pca.transform(df_log_contigs)
	scores = scores[:, 0:2] #selection n principal components
    pca_df = pd.DataFrame(scores)
    pca_df.to_csv(outfile)
    #pca_df.to_csv(outfile,index=False,header=False,columns=None,sep=' ')
    return scores

if __name__=="__main__":
    import sys
    signature_file = sys.argv[1]
    outfile = sys.argv[2]
    pca_results = pca_signatures(signature_file,outfile)
    # pca_df = pd.DataFrame(pca_results)
    # pca_df.to_csv(outfile)
    # print(pca_results)
    

    
