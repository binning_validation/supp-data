#!/usr/bin/env python
import numpy as np
import pandas as pd
from itertools import product
from Bio import SeqIO
# optimized sliding window function from
# http://stackoverflow.com/a/7636587
from itertools import tee
from random import sample

# https://www.bioinformatics.org/sms/iupac.html
def dna_plus(seq):
    bases = ['A','G']
    seq=seq.replace("R", str(sample(bases,  1)[0]))
    bases = ['C','T']
    seq=seq.replace("Y", str(sample(bases,  1)[0]))
    bases = ['G','C']
    seq=seq.replace("S", str(sample(bases,  1)[0]))
    bases = ['A','T']
    seq=seq.replace("W", str(sample(bases,  1)[0]))
    bases = ['G','T']
    seq=seq.replace("K", str(sample(bases,  1)[0]))
    bases = ['A','C']
    seq=seq.replace("M", str(sample(bases,  1)[0]))
    bases = ['C','G','T']
    seq=seq.replace("B", str(sample(bases,  1)[0]))
    bases = ['A','G','T']
    seq=seq.replace("D", str(sample(bases,  1)[0]))
    bases = ['A','C','T']
    seq=seq.replace("H", str(sample(bases,  1)[0]))
    bases = ['A','C','G']
    seq=seq.replace("V", str(sample(bases,  1)[0]))
    bases = ['A','T','G','C']
    seq=seq.replace("N", str(sample(bases,  1)[0]))
    return seq

def window(seq,n):
    seq = dna_plus(seq)
    els = tee(seq,n)
    for i,el in enumerate(els):
        for _ in range(i):
            next(el, None)
    return zip(*els)

def generate_feature_mapping(kmer_len):
    BASE_COMPLEMENT = {"A":"T","T":"A","G":"C","C":"G"}
    kmer_hash = {}
    counter = 0
    for kmer in product("ATGC",repeat=kmer_len):
        kmer = ''.join(kmer)
        if kmer not in kmer_hash:
            kmer_hash[kmer] = counter
            rev_compl = ''.join([BASE_COMPLEMENT[x] for x in reversed(kmer)])
            kmer_hash[rev_compl] = counter
            counter += 1
    return kmer_hash

# For fasta paired-end interleaved
def generate_features_from_fasta_PEi(fasta_file,kmer_len,outfile):
    kmer_dict = generate_feature_mapping(kmer_len)
    seqs = SeqIO.parse(fasta_file,"fasta")
    nr_datapoints = int((len(SeqIO.index(fasta_file,"fasta")))/2)
    #Initialize feature vectors. NxD where N is number of datapoints, D is number of dimentions
    contigs = np.zeros((nr_datapoints,max(kmer_dict.values())+1))
    contigs_id = []
    j = 0
    for i,seq in enumerate(seqs):
        if i % 2 == 0:
            contigs_id.append(seq.id)

        for kmer_tuple in window(str(seq.seq).upper(),kmer_len):
            contigs[j,kmer_dict["".join(kmer_tuple)]] += 1
        
        if not i % 2 == 0:
            j+=1

    df = pd.DataFrame(contigs,index=contigs_id)
    df.to_csv(outfile)

# For fasta paired-end splitted files
def generate_features_from_fasta_PE(fasta_file1,fasta_file2,kmer_len,outfile):
    kmer_dict = generate_feature_mapping(kmer_len)
    seqs1 = SeqIO.parse(fasta_file1,"fasta")
    seqs2 = SeqIO.parse(fasta_file2,"fasta")
    nr_datapoints = len(SeqIO.index(fasta_file1,"fasta"))
    #Initialize feature vectors. NxD where N is number of datapoints, D is number of dimentions
    contigs = np.zeros((nr_datapoints,max(kmer_dict.values())+1))
    contigs_id = []
    for i,seq in enumerate(seqs1):
        contigs_id.append(seq.id)
        for kmer_tuple in window(str(seq.seq).upper(),kmer_len):
            contigs[i,kmer_dict["".join(kmer_tuple)]] += 1

    for j,seq in enumerate(seqs2):
        for kmer_tuple in window(str(seq.seq).upper(),kmer_len):
            contigs[j,kmer_dict["".join(kmer_tuple)]] += 1

    df = pd.DataFrame(contigs,index=contigs_id)
    df.to_csv(outfile)

# For fasta single-end
def generate_features_from_fasta_SE(fasta_file,kmer_len,outfile):
    kmer_dict = generate_feature_mapping(kmer_len)
    seqs = SeqIO.parse(fasta_file,"fasta")
    nr_datapoints = len(SeqIO.index(fasta_file,"fasta"))
    #Initialize feature vectors. NxD where N is number of datapoints, D is number of dimentions
    contigs = np.zeros((nr_datapoints,max(kmer_dict.values())+1))
    contigs_id = []
    for i,seq in enumerate(seqs):
        contigs_id.append(seq.id)
        for kmer_tuple in window(str(seq.seq).upper(),kmer_len):
            contigs[i,kmer_dict["".join(kmer_tuple)]] += 1
    df = pd.DataFrame(contigs,index=contigs_id)
    df.to_csv(outfile)
#    print contigs_id
#    contigs_id = np.array(contigs_id).reshape((-1,1))
#    print contigs_id.shape
#    print contigs.shape

#    np.savetxt(outfile,np.hstack((contigs_id,contigs)),delimiter=",")
    
if __name__=="__main__":
    import sys
    outfile = sys.argv[3]
    kmer_len = int(sys.argv[2])
    read_type = sys.argv[1]
    fasta_file1 = sys.argv[4]

    if read_type == 'se':
        generate_features_from_fasta_SE(fasta_file1,kmer_len,outfile)
    if read_type == 'pe':
        fasta_file2 = sys.argv[5]
        generate_features_from_fasta_PE(fasta_file1,fasta_file2,kmer_len,outfile)
    if read_type == 'pi':
        generate_features_from_fasta_PEi(fasta_file1,kmer_len,outfile)

    # fasta_to_features.py se 4 [outfile] [infile]
	# fasta_to_features.py pi 4 [outfile] [infile]
	# fasta_to_features.py pe 4 [outfile] [infile1] [infile2]