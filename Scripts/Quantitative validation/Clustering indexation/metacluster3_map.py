# -*- coding: utf-8 -*-

import os
import sys
import re

output = open(sys.argv[2],'w')
output.write(',0\n')
inputfile = sys.argv[1]
filename = os.path.basename(inputfile)
name = re.sub(r'-[\x20-\x7E]*', '', filename)
dirname = os.path.dirname(inputfile)
filtered = open(filename+'_filtered','w')
nr_reads = int(sys.argv[3])
nfiles = int(sys.argv[4])
                    
lista = [None] * nr_reads
t = 0
for j in range(nfiles):
    if j < 10:
        namef=dirname+'/'+name+'-out00'+str(j)+'.fa'
    else:
        namef=dirname+'/'+name+'-out0'+str(j)+'.fa'
    
    if os.stat(namef).st_size != 0:
        t = t+1
        with open(namef) as f:
                for i, l in enumerate(f):
                    if l[0] == '>':
                        w = l
                        w = w.replace('>r','')
                        w = re.sub(r'.[0-9] [\x20-\x7E]*$', '', w)
                        wn = int(w) 
                        wn = wn - 1
                        lista[wn]=t
                    
f.close()

t = 0
for k in range(nr_reads):
    if lista[k] == None:
        filtered.write(str(k)+'\n')
    else:
        output.write(str(t)+','+str(lista[k])+'\n')
        t = t+1

output.close()
filtered.close()
#metacluster3_map.py .clusters file.clust nr_reads nfiles