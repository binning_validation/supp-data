#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import re
output = open(sys.argv[2],'w') #file.clust [output]
output.write(',0\n')

with open(sys.argv[1]) as f: #file.fna.clusters.csv
        for i, l in enumerate(f):
            if l[0] == '>':
                n = i
                w = re.sub(r'. *[>r][0-9]*.[0-9],', '', l)
                w = int(w)+1
                w = str(i)+','+str(w)
                output.write(w+'\n')
               
f.close()
output.close()
#metaprob_map.py file.fna.clusters.csv file.clust