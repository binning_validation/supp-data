# -*- coding: utf-8 -*-
import os
import sys
import re

output = open(sys.argv[2],'w')
output.write(',0\n')
inputfile = sys.argv[1]
filename = os.path.basename(inputfile)
name = re.sub(r'_[\x20-\x7E]*', '', filename)
dirname = os.path.dirname(inputfile)
pair = sys.argv[4]
nr_reads = 0
nc = int(sys.argv[3])
for j in range(nc):
    with open(dirname+'/'+name+'_filtered_species_'+str(j)) as f:
        for i, l in enumerate(f):
                    if l[0] == '>':
                        nr_reads = nr_reads + 1
                    
if pair == 'pe':
    nr_reads = int(nr_reads/2)

lista = [None] * nr_reads
for j in range(nc):
    with open(dirname+'/'+name+'_filtered_species_'+str(j)) as f:
            for i, l in enumerate(f):
                if l[0] == '>':
                    w = l
                    w = w.replace('>r','')
                    w = re.sub(r'.[0-9] [\x20-\x7E]*$', '', w)
                    wn = int(w) 
                    wn = wn - 1
                    lista[wn]=j+1
                    
f.close()

for k in range(nr_reads):
    output.write(str(k)+','+str(lista[k])+'\n')

output.close()
#mbbc_map.py .clusters file.clust 2 pe