# -*- coding: utf-8 -*-
import os
import sys
import re

output = open(sys.argv[2],'w')
output.write(',0\n')
inputfile = sys.argv[1]
filename = os.path.basename(inputfile)
filtered = open(filename+'_filtered','w')
nr_reads = 0
                   
nr_reads = int(sys.argv[3])
j = 0
lista = [None] * nr_reads
with open(inputfile) as f:
        for i, l in enumerate(f):
            if l[0] == 'C':
                j = j+1
            if l[0] == '>':
                w = l
                w = w.replace('>r','')
                w = re.sub(r'.[0-9] [\x20-\x7E]*$', '', w)
                wn = int(w) 
                wn = wn - 1
                lista[wn]=j
                    
f.close()

t = 0
for k in range(nr_reads):
    if lista[k] == None:
        filtered.write(str(k)+'\n')
    else:
        output.write(str(t)+','+str(lista[k])+'\n')
        t = t+1

output.close()
filtered.close()
#metacluster5_map.py .clusters file.clust nr_reads