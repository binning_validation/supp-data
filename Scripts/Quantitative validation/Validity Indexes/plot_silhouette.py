import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np

def plotSilhouette(sample_silhouette_values,cluster_labels,outname):
    # Generating the sample data from make_blobs
    # This particular setting has one distinct cluster and 3 clusters placed close
    # together.
    
    #for n_clusters in range_n_clusters:
    n_clusters = int(np.max(cluster_labels))
    # Create a subplot with 1 row and 2 columns
    fig, (ax1) = plt.subplots(1)
    fig.set_size_inches(8, 7)
    
    # The 1st subplot is the silhouette plot
    # The silhouette coefficient can range from -1, 1 but in this example all
    # lie within [-0.1, 1]
    ax1.set_xlim([-0.1, 1])
    # The (n_clusters+1)*10 is for inserting blank space between silhouette
    # plots of individual clusters, to demarcate them clearly.
    ax1.set_ylim([0, len(cluster_labels) + (n_clusters + 1) * 10])
    
    
    
#    # Compute the silhouette scores for each sample
#    sample_silhouette_values = silhouette_samples(X, cluster_labels)
#    
#    # The silhouette_score gives the average value for all the samples.
#    # This gives a perspective into the density and separation of the formed
#    # clusters
    silhouette_avg = np.sum(sample_silhouette_values)/len(sample_silhouette_values)
#    print("For n_clusters =", n_clusters,
#          "The average silhouette_score is :", silhouette_avg)

    y_lower = 10
    for i in range(n_clusters):
        # Silhouette clusters
        cluster_items = sample_silhouette_values[cluster_labels == i+1]
        cluster_score = np.sum(cluster_items)/len(cluster_items)
        # Aggregate the silhouette scores for samples belonging to
        # cluster i, and sort them
        ith_cluster_silhouette_values = \
            sample_silhouette_values[cluster_labels == i+1]
    
        ith_cluster_silhouette_values.sort()
    
        size_cluster_i = ith_cluster_silhouette_values.shape[0]
        y_upper = y_lower + size_cluster_i
    
        color = cm.nipy_spectral(float(i+1) / n_clusters)
        ax1.fill_betweenx(np.arange(y_lower, y_upper),
                          0, ith_cluster_silhouette_values,
                          facecolor=color, edgecolor=color, alpha=0.7)
    
        # Label the silhouette plots with their cluster numbers at the middle
        ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i+1))
        ax1.text(0.69, y_lower + 0.5 * size_cluster_i, "score cluster %d: %.5f" % (i+1, cluster_score))
    
        # Compute the new y_lower for next plot
        y_lower = y_upper + 10  # 10 for the 0 samples
    ysize = int((np.size(sample_silhouette_values))/2)
    ax1.text(silhouette_avg+0.01, ysize, "Average score: %.5f" % silhouette_avg)
    ax1.set_xlabel("The silhouette coefficient values")
    ax1.set_ylabel("Cluster label")
    
    # The vertical line for average silhouette score of all the values
    ax1.axvline(x=silhouette_avg, color="red", linestyle="--")
    
    ax1.set_yticks([])  # Clear the yaxis labels / ticks
    ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])
    
    plt.suptitle(("Silhouette analysis for clustering on sample data "
                  "with n_clusters = %d" % n_clusters),
                 fontsize=14, fontweight='bold')
    
#    plt.show()
#    plt.savefig('silhouette.pdf', bbox_inches='tight')
    return plt.savefig(outname, bbox_inches='tight')
