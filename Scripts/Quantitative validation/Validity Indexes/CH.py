# -*- coding: utf-8 -*-

from sklearn.metrics import calinski_harabaz_score
import subprocess
import pandas as pd
import fasta_to_features as ff
import features_to_pca as fp
import sys
import os
import numpy as np

class Unbuffered:

	def __init__(self, stream):
		self.stream = stream

	def write(self, data):
		self.stream.write(data)
		self.stream.flush()
		te.write(data)    # Write the data of stdout here to a text file as well

	def flush(self):
		pass


if __name__=="__main__":
	import time
	start = time.time()
	te = open('log.txt','w')  # File where you need to keep the logs
	sys.stdout=Unbuffered(sys.stdout)
#	if not os.path.exists('subsamples'):
#		os.makedirs('subsamples')
#	if not os.path.exists('features_signatures'):
#		os.makedirs('features_signatures')
#	if not os.path.exists('output'):
#		os.makedirs('output')
	
	print('\n******    Calinski-Harabaz Index     *******')
	print('****** For metagenomic fasta samples *******')
	# Load data and generate distance matrix.
	print('Loading Input file...')
	in_file = sys.argv[1]
#	nr_samples = sys.argv[2]
#	seed = sys.argv[3]
	filename = os.path.basename(in_file)
#	command = ['./seqtk sample -s%s %s %s > %s' % (seed,in_file,nr_samples,'./subsamples/'+filename+'_subsample')]
#	com_out = subprocess.getoutput(command)

	fasta_file = in_file#'./subsamples/'+filename+'_subsample'
#	outfile1 = './features_signatures/'+filename+'.feat'
	outfile2 = '/mnt/f/features_signatures/'+filename+'.sig'
#	print('Generating features (k-mers freq)...')
#	ff.generate_features_from_fasta(fasta_file,4,outfile1)
#
#	print('Generating signatures (PCA) of reads...')
#	fp.pca_signatures(outfile1,outfile2)

	V = pd.read_csv(outfile2)
	V.drop(V.columns[0], axis=1, inplace=True)
	V = V.values
	
	#For metacluster**************
	# F = pd.read_csv(sys.argv[3],header=None)#sys.argv[3])
	# F = F.values
	# V = np.delete(V, F, 0)
	#******************************
	
	clust = pd.read_csv(sys.argv[2]) # .clust
	clust = clust.values
	clust = clust[:,1]

	# Compute index CH
	CH = calinski_harabaz_score(V, clust)
	print(filename)
	print('Calinski-Harabaz index value: %.3f' % CH)

	time = time.time() - start
	print('Execution Complete in %.2f seconds.' % time)
	