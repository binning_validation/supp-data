# -*- coding: utf-8 -*-

import numpy as np
from scipy.spatial.distance import euclidean, cdist, pdist, squareform
import subprocess
import pandas as pd
import fasta_to_features as ff
import features_to_pca as fp
import sys
import os

class Unbuffered:

	def __init__(self, stream):
		self.stream = stream

	def write(self, data):
		self.stream.write(data)
		self.stream.flush()
		te.write(data)    # Write the data of stdout here to a text file as well

	def flush(self):
		pass

def db_index(X, y):
    """
    Davies-Bouldin index is an internal evaluation method for
    clustering algorithms. Lower values indicate tighter clusters that 
    are better separated.
    """
    # get unique labels
    if y.ndim == 2:
        y = np.argmax(axis=1)
    uniqlbls = np.unique(y)
    n = len(uniqlbls)
    # pre-calculate centroid and sigma
    centroid_arr = np.empty((n, X.shape[1]))
    sigma_arr = np.empty((n,1))
    dbi_arr = np.empty((n,n))
    mask_arr = np.invert(np.eye(n, dtype='bool'))
    for i,k in enumerate(uniqlbls):
        Xk = X[np.where(y==k)[0],...]
        Ak = np.mean(Xk, axis=0)
        centroid_arr[i,...] = Ak
        sigma_arr[i,...] = np.mean(cdist(Xk, Ak.reshape(1,-1)))
    # compute pairwise centroid distances, make diagonal elements non-zero
    centroid_pdist_arr = squareform(pdist(centroid_arr)) + np.eye(n)
    # compute pairwise sigma sums
    sigma_psum_arr = squareform(pdist(sigma_arr, lambda u,v: u+v))
    # divide 
    dbi_arr = np.divide(sigma_psum_arr, centroid_pdist_arr)
    # get mean of max of off-diagonal elements
    dbi_arr = np.where(mask_arr, dbi_arr, 0)
    dbi = np.mean(np.max(dbi_arr, axis=1))
    return dbi

if __name__=="__main__":
	import time
	start = time.time()
	te = open('log.txt','w')  # File where you need to keep the logs
	sys.stdout=Unbuffered(sys.stdout)
#	if not os.path.exists('subsamples'):
#		os.makedirs('subsamples')
#	if not os.path.exists('features_signatures'):
#		os.makedirs('features_signatures')
#	if not os.path.exists('output'):
#		os.makedirs('output')
	
	print('\n******     Davies Bouldin Index      *******')
	print('****** For metagenomic fasta samples *******')
	# Load data and generate distance matrix.
	print('Loading Input file...')
	in_file = sys.argv[1]
#	nr_samples = sys.argv[2]
#	seed = sys.argv[3]
	filename = os.path.basename(in_file)
#	command = ['./seqtk sample -s%s %s %s > %s' % (seed,in_file,nr_samples,'./subsamples/'+filename+'_subsample')]
#	com_out = subprocess.getoutput(command)

	fasta_file = in_file#'./subsamples/'+filename+'_subsample'
#	outfile1 = './features_signatures/'+filename+'.feat'
	outfile2 = '/mnt/f/features_signatures/'+filename+'.sig'
#	print('Generating features (k-mers freq)...')
#	ff.generate_features_from_fasta(fasta_file,4,outfile1)
#
#	print('Generating signatures (PCA) of reads...')
#	fp.pca_signatures(outfile1,outfile2)

	V = pd.read_csv(outfile2)
	V.drop(V.columns[0], axis=1, inplace=True)
	V = V.values

	#For metacluster**************
	# F = pd.read_csv(sys.argv[3],header=None)#sys.argv[3])
	# F = F.values
	# V = np.delete(V, F, 0)
	#******************************

	clust = pd.read_csv(sys.argv[2])
	clust = clust.values
	clust = clust[:,1]

	# Compute index DB
	#DB = davies_bouldin_score(V, clust)
	DB = db_index(V, clust)
	print(filename)
	print('Davies-Bouldin index value: %.3f' % DB)

	time = time.time() - start
	print('Execution Complete in %.2f seconds.' % time)
	