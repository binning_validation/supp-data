# Example of commands sequence for reproducibility

## Sequence clustering
INPUT: R1.fna

Binning tools commands:
```bash
java -jar -XX:MaxHeapFreeRatio=70 -Xmx20g MBBC.jar -i /home/user/datasets/R1.fna -m 10 -r 1
./MetaProb -si /home/user/datasets/R1.fna -eK
./metaCluster-3.0 /home/user/datasets/R1.fna
```

OUTPUT: sequence bins

## Quantitative validation
### Sequence vectorization:

step 1: Fasta sample to feature
```bash
python3 fasta_to_features.py se 4 /home/user/features_signatures/R1.fna.feat /home/user/datasets/R1.fna
```
step 2: Feature to signature file
```bash
python3 features_to_pca.py /home/user/features_signatures/R1.fna.feat /home/user/features_signatures/R1.fna.sig
```
### Clustering indexation:
```bash
python3 mbbc_map.py /home/user/datasets/binning_outs/MBBC_out/R1.fna_filtered_species_0 R1.clust 1 se
python3 metaprob_map.py /home/user/datasets/binning_outs/MetaProb_out/R1.fna.clusters.csv R1.clust
python3 metacluster3_map.py /home/user/datasets/binning_outs/Metaprob3.0_out/R1.fna-out000.fa R1.clust 82960 2
```
### Validity indexes: ('R1.fna.sig' file is used here as input, called by prefix 'R1.fna')
```bash
python3 CH.py R1.fna /home/user/map_clusters/mbbc/R1.clust
python3 CH.py R1.fna /home/user/map_clusters/metaprob/R1.clust
python3 CH.py R1.fna /home/user/map_clusters/metacluster3/R1.clust /home/user/map_clusters/metacluster3/R1.fna-out000.fa_filtered
	
python3 DB.py R1.fna /home/user/map_clusters/mbbc/R1.clust
python3 DB.py R1.fna /home/user/map_clusters/metaprob/R1.clust
python3 DB.py R1.fna /home/user/map_clusters/metacluster3/R1.clust /home/user/map_clusters/metacluster3/R1.fna-out000.fa_filtered
	
python3 silhouette.py /home/user/features_signatures/R1.fna.sig /home/user/map_clusters/mbbc/R1.clust R1_mbbc.pdf R1_mbbc.sil
python3 silhouette.py /home/user/features_signatures/R1.fna.sig /home/user/map_clusters/metaprob/R1.clust R1_metaprob.pdf R1_metaprob.sil
python3 silhouette.py /home/user/features_signatures/R1.fna.sig /home/user/map_clusters/metacluster3/R1.clust R1_metacluster.pdf R1_metacluster.sil /home/user/map_clusters/metacluster3/R1.fna-out000.fa_filtered
```	
OUTPUT: scores and charts
	
## Qualitative validation
### Bins (R1.clust) to fasta files:
```bash
python3 clust2fasta.py /home/user/map_clusters/mbbc/R1.clust mbbc se /home/user/datasets/R1.fna
python3 clust2fasta.py /home/user/map_clusters/metacluster3/R1.clust metacluster se /home/user/datasets/R1.fna /home/user/map_clusters/metacluster3/R1.fna-out000.fa_filtered
python3 clust2fasta.py /home/user/map_clusters/metaprob/R1.clust metaprob se /home/user/datasets/R1.fna
```		
### Assembly (1 bin from MBBC + 10 bins from MetaProb + 1 bin from MetaCluster):
```bash
./megahit -t 8 -m 0.9 -r /home/user/bins_R/mbbc/R1.fna_cluster1_mbbc.fa -o /home/user/megahitR/mbbc/R/R1_1
./megahit -t 8 -m 0.9 -r /home/user/bins_R/metaprob/R1.fna_cluster1_metaprob.fa -o /home/user/megahitR/metaprob/R/R1_1
./megahit -t 8 -m 0.9 -r /home/user/bins_R/metaprob/R1.fna_cluster2_metaprob.fa -o /home/user/megahitR/metaprob/R/R1_2
./megahit -t 8 -m 0.9 -r /home/user/bins_R/metaprob/R1.fna_cluster3_metaprob.fa -o /home/user/megahitR/metaprob/R/R1_3
./megahit -t 8 -m 0.9 -r /home/user/bins_R/metaprob/R1.fna_cluster4_metaprob.fa -o /home/user/megahitR/metaprob/R/R1_4
./megahit -t 8 -m 0.9 -r /home/user/bins_R/metaprob/R1.fna_cluster5_metaprob.fa -o /home/user/megahitR/metaprob/R/R1_5
./megahit -t 8 -m 0.9 -r /home/user/bins_R/metaprob/R1.fna_cluster6_metaprob.fa -o /home/user/megahitR/metaprob/R/R1_6
./megahit -t 8 -m 0.9 -r /home/user/bins_R/metaprob/R1.fna_cluster7_metaprob.fa -o /home/user/megahitR/metaprob/R/R1_7
./megahit -t 8 -m 0.9 -r /home/user/bins_R/metaprob/R1.fna_cluster8_metaprob.fa -o /home/user/megahitR/metaprob/R/R1_8
./megahit -t 8 -m 0.9 -r /home/user/bins_R/metaprob/R1.fna_cluster9_metaprob.fa -o /home/user/megahitR/metaprob/R/R1_9
./megahit -t 8 -m 0.9 -r /home/user/bins_R/metaprob/R1.fna_cluster10_metaprob.fa -o /home/user/megahitR/metaprob/R/R1_10
./megahit -t 8 -m 0.9 -r /home/user/bins_R/metacluster/R1.fna_cluster1_metacluster.fa -o /home/user/megahitR/metacluster/R/R1_1
```
OUTPUT: contigs
	
### CheckM (Content of '.../contigs_R1' = 'final.contigs.fa' files from Megahit outputs):
```bash
checkm lineage_wf -t 8 -x fa /home/user/contigs_R/mbbc/contigs_R1 /home/user/validation/checkm/mbbc/R1
checkm lineage_wf -t 8 -x fa /home/user/contigs_R/metaprob/contigs_R1 /home/user/validation/checkm/metaprob/R1
checkm lineage_wf -t 8 -x fa /home/user/contigs_R/metacluster/contigs_R1 /home/user/validation/checkm/metacluster/R1
```
### MetaQUAST:
```bash
metaquast.py -t 8 -o /home/user/validation/metaquast/mbbc/R1 /home/user/contigs_R/mbbc/contigs_R1/*.fa
metaquast.py -t 8 -o /home/user/validation/metaquast/metaprob/R1 /home/user/contigs_R/metaprob/contigs_R1/*.fa
metaquast.py -t 8 -o /home/user/validation/metaquast/metacluster/R1 /home/user/contigs_R/metacluster/contigs_R1/*.fa
```
OUTPUT: reports of CheckM and MetaQUAST analysis
