#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from Bio import SeqIO
import sys
import os

def clusters_files(clust, binner, type, fasta_file, fasta_file2, filt):
#    binner = 'metacluster'
#    fasta_file = '/home/user/samples/R1.fna'
    if type == 'pe':
        name1 = os.path.basename(fasta_file)
        seqs1 = list(SeqIO.parse(fasta_file,"fasta"))
        name2 = os.path.basename(fasta_file2)
        seqs2 = list(SeqIO.parse(fasta_file2,"fasta"))
    else:
        name = os.path.basename(fasta_file)
        seqs = list(SeqIO.parse(fasta_file,"fasta"))
    #cout = open('outp', "w")
    clust = pd.read_csv(clust,header=0,index_col=0)
    clust = clust.values
    
    for k in range(np.max(clust)):
            if type == 'pe':
                exec('cf%d_1 = open("%s_cluster%d_%s_1.fa", "w")' % (k+1,name1,k+1,binner))
                exec('cf%d_2 = open("%s_cluster%d_%s_2.fa", "w")' % (k+1,name2,k+1,binner))
            else:
                exec('cf%d = open("%s_cluster%d_%s.fa", "w")' % (k+1,name,k+1,binner))
    
    if binner == 'metacluster':
        filt = pd.read_csv(filt,header=0)
        filt = filt.values
        size = np.size(clust)+np.size(filt)
        t = 0
        for i in range(size):
            if i in filt:
                pass
                #cout.write(str(i)+' '+str(t)+'\n')
            else:
                nc = clust[t][0]
                if type == 'pe':
                    line11 = '>'+str(seqs1[i].description)+'\n'
                    line12 = str(seqs1[i].seq)+'\n'
                    line21 = '>'+str(seqs2[i].description)+'\n'
                    line22 = str(seqs2[i].seq)+'\n'
                    exec('cf%d_1.write(line11)' % (nc))
                    exec('cf%d_1.write(line12)' % (nc))
                    exec('cf%d_2.write(line21)' % (nc))
                    exec('cf%d_2.write(line22)' % (nc))
                else:
                    line1 = '>'+str(seqs[i].description)+'\n'
                    line2 = str(seqs[i].seq)+'\n'
                    exec('cf%d.write(line1)' % (nc))
                    exec('cf%d.write(line2)' % (nc))
                if t < np.size(clust)-1:
                    t = t + 1
                
    else:
        for i in range(np.size(clust)):
            nc = clust[i][0]
            if type == 'pe':
                line11 = '>'+str(seqs1[i].description)+'\n'
                line12 = str(seqs1[i].seq)+'\n'
                line21 = '>'+str(seqs2[i].description)+'\n'
                line22 = str(seqs2[i].seq)+'\n'
                exec('cf%d_1.write(line11)' % (nc))
                exec('cf%d_1.write(line12)' % (nc))
                exec('cf%d_2.write(line21)' % (nc))
                exec('cf%d_2.write(line22)' % (nc))
            else:
                line1 = '>'+str(seqs[i].description)+'\n'
                line2 = str(seqs[i].seq)+'\n'
                exec('cf%d.write(line1)' % (nc))
                exec('cf%d.write(line2)' % (nc))

if __name__=="__main__":
    if sys.argv[2] == 'metacluster':
        if len(sys.argv) < 7:
            clusters_files(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], 'None', sys.argv[5]) #if metacluster [se]
        else:
            clusters_files(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6]) #if metacluster [pe]
    else:
        if len(sys.argv) > 5:
            clusters_files(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], 'None') # if mbbc or metaprob [pe]
        else:
            clusters_files(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], 'None', 'None') # if mbbc or metaprob [se]

# clust2fasta.py file.clust binner file.fna [single-end, metaprob, mbbc]
# clust2fasta.py file.clust binner file_1.fna file_2.fna [paired-end, metaprob, mbbc]
# clust2fasta.py file.clust binner file.fna file_filtered [metacluster 3.0]
# clust2fasta.py file.clust binner file_1.fna file_2.fna file_filtered [metacluster 5.0]
