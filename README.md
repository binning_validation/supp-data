# Supplementary Material of the study 'On Clustering Validation in Metagenomics Sequence Binning'


*  Dataset_description: describes the dataset in more detail.

*  Tables: Show additional tables.

*  The spreadsheets present some of the analysis results.

*  Datasets folder: the samples used in the research.

*  Scripts folder: the scripts developed and used in the analyzes, and usage instructions.

